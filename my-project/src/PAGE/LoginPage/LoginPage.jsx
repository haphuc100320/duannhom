
import { Button, Checkbox, Form, Input } from 'antd';
import { FaLock } from 'react-icons/fa';
import React from 'react';

const App = () => {
  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

   return (
    <div className='bg-lime-100 h-screen w-screen p-3'>
    <div className='text-center bg-cyan-100  mx-auto rounded-xl p-10 w-1/4'>
        <div className='inline-block border-current  h-full text-6xl '>
    <h3 className=' '> <FaLock  /> </h3>
    </div>
        <h4 className='text-4xl font-medium'>Sign In With</h4>
        <hr className='bg-black mt-5' />
        <div className='h-1/4 w-80 mr-3 overflow-hidden'>
            {/* <LoginAnimate/> */}
        </div>
        <div className='w-full py-5'>
        <Form
        // className='w-full'
            name="basic"
            layout='vertical'
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 24,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >
            <Form.Item
                label="Tài Khoản"
                name="taiKhoan"
                rules={[
                    {
                        required: true,
                        message: 'Please input your username!',
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
            className='ml-3'
                label="Mật Khẩu"
                name="matKhau"
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>
            <div className=''>
                <button className='rounded px-5 py-2 text-white bg-black w-1/2 mt-4  '>Login</button>
            </div>
        </Form>
    
        </div>
    </div>
</div>
  );
};

export default App;